## yace 

This is a prometheus exporter for AWS CloudWatch

It contains the yace configuration template : *config.yml* for the yace exporter metrics
            the yace service template : *yace.service* to run the exporter on the server 


## To use this role

* Add in the *requirements.yml* file what follows:

```yaml

- src: https://gitlab.com/myelefant1/ansible-roles3/yace-exporter.git
  scm: git
- src: https://gitlab.com/myelefant1/ansible-roles3/github_release_install.git
  scm: git


```

* copy the configuration file *config.yml* in the templates files 

* Write a plabook file:

```yaml
- hosts: prometheus
  become: true
  become_user: root
  roles:
    - role: yace-exporter
      yace_exporter_configuration_template: templates/config.yml
```

## Tests 

```bach
./test/bach_unit tests/tests_yace_exporter
```
